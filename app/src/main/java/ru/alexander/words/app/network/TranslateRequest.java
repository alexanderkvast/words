package ru.alexander.words.app.network;

import android.os.Handler;
import android.os.Looper;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class TranslateRequest {

    protected static final Executor EXECUTOR = Executors.newCachedThreadPool();

    protected static final String API_KEY = "trnsl.1.1.20141112T093534Z.cc5f9c769a4c51fe.e0d35307255d0da85b86d666dce8df3289bbc6bb";

    protected static final String URL = "https://translate.yandex.net/api/v1.5/tr.json/translate";

    public String text;

    public TranslateRequest(String text) {
        this.text = text;
    }

    public void Send(final OnResponseCallback callback) {
        EXECUTOR.execute(new Runnable() {
            @Override
            public void run() {
                SendImpl(callback);
            }
        });
    }

    protected void SendImpl(final OnResponseCallback callback) {
        try {

            HttpClient client = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(URL);

            List<NameValuePair> args = new ArrayList<NameValuePair>(3);
            args.add(new BasicNameValuePair("key", API_KEY));
            args.add(new BasicNameValuePair("text", text));
            args.add(new BasicNameValuePair("lang", "ru"));
            httpPost.setEntity(new UrlEncodedFormEntity(args, "UTF-8"));

            HttpResponse response = client.execute(httpPost);
            String responseString = EntityUtils.toString(response.getEntity(), "UTF-8");
            JSONObject jsonObject = new JSONObject(responseString);

            final String translation = jsonObject.optJSONArray("text").getString(0);

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    callback.OnResponse(translation);
                }
            });
        } catch (Exception ex) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    callback.OnError();
                }
            });
        }
    }

    public static interface OnResponseCallback {
        void OnResponse(String translation);
        void OnError();
    }

}
