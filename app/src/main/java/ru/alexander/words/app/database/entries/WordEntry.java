package ru.alexander.words.app.database.entries;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class WordEntry implements BaseColumns {

    public static final String TABLE_NAME = "word";
    public static final String COLUMN_NAME_WORD = "word";
    public static final String COLUMN_NAME_TRANSLATION = "translation";

    public long _id = 0;
    public String word;
    public String translation;

    public WordEntry(String word, String translation) {
        this.word = word;
        this.translation = translation;
    }

    public void save(SQLiteDatabase db) {

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_WORD, word);
        values.put(COLUMN_NAME_TRANSLATION, translation);

        if (_id == 0) {
            _id = db.insert(TABLE_NAME, null, values);
            return;
        }

        String selection = _ID + " LIKE ?";
        String[] selectionArgs = {String.valueOf(_id)};

        db.update(TABLE_NAME, values, selection, selectionArgs);
    }

    // Static methods:

    public static Cursor getRowIds(SQLiteDatabase db) {
        return db.rawQuery(
                "SELECT " + _ID +
                        " FROM " + TABLE_NAME +
                        " ORDER BY " + _ID +
                        " DESC", null);
    }

    public static Cursor findRowIdsWithText(SQLiteDatabase db, String searchText) {
        searchText = "%" + searchText + "%";

        return db.rawQuery(
                "SELECT " + _ID +
                        " FROM " + TABLE_NAME +
                        " WHERE " + COLUMN_NAME_WORD + " LIKE ?" +
                        " OR " + COLUMN_NAME_TRANSLATION + " LIKE ?" +
                        " ORDER BY " + _ID + " DESC",
                new String[]{searchText, searchText});
    }

    public static Cursor getRowById(SQLiteDatabase db, long _id) {
        return db.rawQuery(
                "SELECT * FROM " + TABLE_NAME +
                        " WHERE " + _ID + " = ?",
                new String[]{Long.toString(_id)});
    }

}
