package ru.alexander.words.app;

import android.app.Application;
import ru.alexander.words.app.database.Database;

public class App extends Application {

    public static Database database;

    @Override
    public void onCreate() {
        super.onCreate();

        database = new Database(this);
    }

}