package ru.alexander.words.app;

import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.melnykov.fab.FloatingActionButton;
import ru.alexander.words.app.database.entries.WordEntry;
import ru.alexander.words.app.network.TranslateRequest;

public class MainActivity extends ActionBarActivity {

    protected ListView wordsListView;
    protected FloatingActionButton floatingActionButton;

    protected WordsAdapter wordsAdapter = new WordsAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        wordsListView = (ListView) findViewById(R.id.wordsListView);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);

        //

        wordsListView.setAdapter(wordsAdapter);

        floatingActionButton.attachToListView(wordsListView);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddAlert();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);

        searchView.setIconifiedByDefault(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                wordsAdapter.setSearchString(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                wordsAdapter.setSearchString(s);
                return false;
            }
        });

        return true;
    }

    protected void showAddAlert() {
        floatingActionButton.hide(true);

        View alertView = View.inflate(this, R.layout.alert_add_word, null);
        final EditText wordEditText = (EditText) alertView.findViewById(R.id.wordEditText);

        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.add_title)
                .setView(alertView)
                .setNegativeButton(R.string.add_negative_button, null)
                .setPositiveButton(R.string.add_positive_button, null)
                .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button positiveButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String word = wordEditText.getText().toString();
                        if (word.length() == 0) {
                            wordEditText.setError(getResources().getText(R.string.add_empty_word));
                            return;
                        }

                        Toast.makeText(MainActivity.this, R.string.translate_fetching, Toast.LENGTH_SHORT).show();

                        new TranslateRequest(word)
                                .Send(new TranslateRequest.OnResponseCallback() {
                                    @Override
                                    public void OnResponse(String translation) {
                                        new WordEntry(word, translation)
                                                .save(App.database.getWritableDatabase());

                                        wordsAdapter.update();
                                    }
                                    @Override
                                    public void OnError() {
                                        Toast.makeText(MainActivity.this, R.string.translate_fetching_error, Toast.LENGTH_SHORT).show();
                                    }
                                });

                        dialog.dismiss();
                    }
                });

                InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
                imm.showSoftInput(wordEditText, 0);
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                floatingActionButton.show(true);
            }
        });

        dialog.show();
    }

    protected class WordsAdapter extends BaseAdapter {

        protected SQLiteDatabase db = App.database.getReadableDatabase();
        protected Cursor rowIds;
        protected int count;

        protected String searchString;

        public WordsAdapter() {
            update();
        }

        public void setSearchString(String searchString) {
            this.searchString = searchString;
            update();
        }

        public void update() {
            rowIds = searchString == null || searchString.length() == 0
                    ? WordEntry.getRowIds(db)
                    : WordEntry.findRowIdsWithText(db, searchString);

            count = rowIds.getCount();
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return count;
        }

        @Override
        public Object getItem(int position) {
            if (rowIds.moveToPosition(position)) {
                long rowId = rowIds.getLong(0);
                return WordEntry.getRowById(db, rowId);
            } else {
                return null;
            }
        }

        @Override
        public long getItemId(int position) {
            if (rowIds.moveToPosition(position)) {
                return rowIds.getLong(0);
            } else {
                return 0;
            }
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            ViewHolder viewHolder;

            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.row_word, parent, false);
                viewHolder = new ViewHolder(view);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            Cursor cursor = (Cursor) getItem(position);
            cursor.moveToFirst();

            viewHolder.wordTextView.setText(cursor.getString(cursor.getColumnIndex(WordEntry.COLUMN_NAME_WORD)));
            viewHolder.translationTextView.setText(cursor.getString(cursor.getColumnIndex(WordEntry.COLUMN_NAME_TRANSLATION)));

            return view;
        }

        public class ViewHolder {
            public TextView wordTextView;
            public TextView translationTextView;

            public ViewHolder(View view) {
                wordTextView = (TextView) view.findViewById(R.id.wordTextView);
                translationTextView = (TextView) view.findViewById(R.id.translationTextView);
            }
        }

    }

}
